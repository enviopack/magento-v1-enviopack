<?php
/**
 * Created by PhpStorm.
 * User: argdev
 * Date: 11/03/17
 * Time: 21:14
 */
class DrubuNet_EnvioPack_Model_Packages extends DrubuNet_EnvioPack_Model_Core
{
    /**
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array();
        $packages = $this->getPackages();

        foreach ($packages as $package) {
            $label = sprintf(
                "%s (%s - %s - %s)",
                $package['nombre'],
                $package['alto'],
                $package['ancho'],
                $package['largo']
            );
            $result[] = array('value'=> $package['id'], 'label'=> $label);
        }

        return $result;
    }

    /**
     * Get Packages from EnvioPack settings
     * @return array
     */
    public function getPackages($packageId = '')
    {
        if ($packageId) $packageId = '/' . $packageId;

        $url = sprintf(
            "%s%s%s",
            $this->_apiUrl,
            'tipos-de-paquetes',
            $packageId
        );

        $params = array(
            'access_token' => $this->getToken()
        );

        return $this->getData($url, $params);
    }
}