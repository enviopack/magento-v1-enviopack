<?php

/**
 * Created by PhpStorm.
 * Company: DrubuNet
 * User: Pablo Garcia
 * Date: 03/03/17
 * Time: 11:28
 */
class DrubuNet_EnvioPack_Model_Observer
{
    /**
     * Save EnvioPack branch in order
     * @param $observer Varien_Event_Observer
     */
    public function customDataSave($observer)
    {
        if(!$this->_isExtensionEnabled()) return;

        /** @var Mage_Core_Model_Session $session */
        $session = Mage::getSingleton("core/session");
        $envioPackBranch = $session->getData('enviopack_branch');

        $event = $observer->getEvent();
        /** @var Mage_Sales_Model_Order $order */
        $order = $event->getOrder();
        $order->setEnviopackBranch($envioPackBranch);
        $session->unsetData('enviopack_branch');
    }

    /**
     * Save EnvioPack branch in quote
     * @param $evt
     */
    public function saveQuoteBefore($evt)
    {
        if(!$this->_isExtensionEnabled()) return;

        /** @var Mage_Core_Model_Session $session */
        $session = Mage::getSingleton("core/session");
        $quote = $evt->getQuote();
        $post = Mage::app()->getFrontController()->getRequest()->getPost();

        $shippingMethod = $post['shipping_method'];
        $shippingInfo = explode('_', $shippingMethod);
        //$shippingName = $shippingInfo[DrubuNet_EnvioPack_Model_Core::SHIPPING_INFO_SHIPPING_NAME];
        if (isset($post['enviopack_branch-'.$shippingMethod])) {
            $shipmentType = strtoupper($shippingInfo[DrubuNet_EnvioPack_Model_Core::SHIPPING_INFO_TYPE]);

            if ($shipmentType == DrubuNet_EnvioPack_Model_Core::ENVIO_SUCURSAL) {
                $envioPackBranch = $post['enviopack_branch-'.$shippingMethod];
                $quote->setEnviopackBranch($envioPackBranch);
                $session->setData('enviopack_branch', $envioPackBranch);
            }
        }

    }

    /**
     * Generate EnvioPack Shipment when Magento Shipment is created
     * @param $observer Varien_Event_Observer
     */
    public function salesOrderShipmentSaveBefore($observer)
    {

//        $shipment = $observer->getEvent()->getShipment();
//        $order = $shipment->getOrder();
//        $orderId = $order->getId();
//
//        /** @var  $order Mage_Sales_Model_Order */
//        $order = Mage::getModel('sales/order')->load($orderId);
//
//        try {
////            $tracking = Mage::helper('fastmail')->setRetiro($order->getId());
////            if(!$tracking)
////                return;
////            $track = Mage::getModel('sales/order_shipment_track')
////                ->setNumber($tracking['tracking'])
////                ->setCarrierCode(substr($order->getShippingMethod(),0,-3))
////                ->setTitle(substr($order->getShippingMethod(),0,-3));
////            $shipment->addTrack($track);
//
//            if (is_null($order->getEnviopackShipmentId())) {
//                //$this->createShipment($order);
//                $envioPackShipmentId = $this->createShipment($order);
//                $order->setEnviopackShipmentId($envioPackShipmentId);
//                Mage::register('prevent_observer',true);
//                $order->save();
//            }
//
//
//        } catch (SoapFault $e) {
//            Mage::log("Error: " . $e);
//            //Mage::throwException(Mage::helper('drubunet_enviopack')->__($e->getMessage()));
//        }
    }

    /**
     * Generate envioPack Shipment when order status change to configured status
     * @param $observer Varien_Event_Observer
     */
    public function statusChangeSaveAfter($observer)
    {
        if(!$this->_isExtensionEnabled()) return;

        if(!Mage::registry('prevent_observer')) {

            $order = $observer->getOrder();
            $orderId = $order->getId();

            /** @var Mage_Sales_Model_Order $order */
            $order = Mage::getModel('sales/order')->load($orderId);

            $shippingMethod = $order->getShippingMethod();

            if (strpos($shippingMethod, 'drubunet_enviopack_') === false) return;

            if (((Mage::helper('drubunet_enviopack')->getConfig('auto_create_shipment') && $order->getStatus() == Mage::helper('drubunet_enviopack')->getConfig('generate_status')) || $order->hasShipments()) && is_null($order->getEnviopackShipmentId())) {

                try {
                    //$envioPackShipmentId = $this->createShipment($order);
                    /** @var DrubuNet_EnvioPack_Model_Shipment $api */
                    $api = Mage::getModel('drubunet_enviopack/shipment');

                    $envioPackOrderId = $order->getEnviopackOrderId();
                    Mage::register('prevent_observer',true);
                    if(!$envioPackOrderId) {
                        $envioPackOrder = $api->createOrder($order);
                        $envioPackOrderId = $envioPackOrder['id'];
                        //$order->setEnviopackOrderId($envioPackOrderId);
                        if (array_key_exists('errors', $envioPackOrder)) {
                            $order->setEnviopackTrackingNumber(json_encode($envioPackOrder, true));
                            //$_order->setShippingMethod($currentShipmentMethod);
                            $order->save();
                        }
                    }
                    //$envioPackShipmentId = 0;

                    if ($envioPackOrderId) {
                        $order->setEnviopackOrderId($envioPackOrderId);
                        $envioPackShipment = $api->createShipment($order, $envioPackOrderId);

                        if (array_key_exists('errors', $envioPackShipment)) {
                            $order->setEnviopackTrackingNumber(json_encode($envioPackShipment, true));
                            $order->save();
                        } else {
                            $order->setEnviopackTrackingNumber('');
                            $envioPackShipmentId = $envioPackShipment['id'];
                            $order->setEnviopackShipmentId($envioPackShipmentId);
                            $order->save();
                            $this->createMagentoShipment($order);
                        }
                    }

//                    Mage::register('prevent_observer',true);
//                    if ($envioPackShipmentId) {
//                        $order->setEnviopackShipmentId($envioPackShipmentId);
//                        $order->save();
//                        $this->createMagentoShipment($order);
//                    } else {
//                        $order->save();
//                    }
                } catch (Exception $e) {
                    Mage::log("Error: " . $e, null, 'envioPack.log');
                }

            }

        }
    }

    /**
     * Create Magento Shipment
     * @param Mage_Sales_Model_Order $order
     */
    private function createMagentoShipment($order)
    {
        /** @var DrubuNet_EnvioPack_Helper_Data $helper */
        $helper = Mage::helper('drubunet_enviopack');

        $helper->createMagentoShipment($order);
    }

    private function _isExtensionEnabled()
    {
        return Mage::helper('drubunet_enviopack')->getConfig('active');
    }
}