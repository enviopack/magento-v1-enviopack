<?php

/**
 * Modified by Atom.
 * User: Santiago de la Fuente
 * Company: Enviopack
 * Date: 07/09/2020
 */
class DrubuNet_EnvioPack_Model_Shipment extends DrubuNet_EnvioPack_Model_Core
{
    /**
     * Instantiate the model with the proper credentials
     *
     * DrubuNet_EnvioPack_Model_Rates constructor.
     */
    public function __construct()
    {
        parent::__construct();

    }

    /**
     * Create EnvioPack Order
     * @param $order Mage_Sales_Model_Order
     * @return array
     */
    public function createOrder($order)
    {
        /** @var DrubuNet_EnvioPack_Helper_Data $helper */
        $helper = Mage::helper('drubunet_enviopack');

        $url = sprintf(
            "%s%s?access_token=%s",
            $this->_apiUrl,
            'pedidos',
            $this->getToken()
        );

        /** @var  $shippingAddress Mage_Sales_Model_Order_Address */
        $shippingAddress = $order->getShippingAddress();

        $content = array(
            'id_externo' => (int)$order->getIncrementId(),
            'nombre' => $shippingAddress->getFirstname(),
            'apellido' => $shippingAddress->getLastname(),
            'email' => $shippingAddress->getEmail(),
            'telefono' => $shippingAddress->getTelephone(),
            'celular' => '',
            'monto' => $order->getGrandTotal(),
            'fecha_alta' => $order->getCreatedAt(),//'2016-04-26T13:52:00-0300',
            'pagado' => true,
            'provincia' => $helper->mapProvince($shippingAddress->getRegion()),
            'localidad' => $shippingAddress->getCity()
        );

        return $this->sendData($url, $content);
    }

    /**
     * Create EnvioPack Shipment
     * @param $order Mage_Sales_Model_Order
     * @param $envioPackId int
     * @return array
     */
    public function createShipment($order, $envioPackId)
    {

        $url = sprintf(
            "%s%s?access_token=%s",
            $this->_apiUrl,
            'envios',
            $this->getToken()
        );

        /** @var  $shippingAddress Mage_Sales_Model_Order_Address */
        //$shippingAddress = $order->getShippingAddress();

        $content = $this->prepareData($order, $envioPackId);

        return $this->sendData($url, $content);
    }

    /**
     * Get localities by province id
     * @param $provinceId
     * @return array
     */
    public function getLocalities($provinceId)
    {
        $url = sprintf(
            "%s%s",
            $this->_apiUrl,
            'localidades'
        );

        $params = array(
            'access_token' => $this->getToken(),
            'id_provincia' => $provinceId
        );

        return $this->getData($url, $params);

    }

//    public function getShipments()
//    {
//        $url = sprintf(
//            "%s%s",
//            $this->_apiUrl,
//            'pedidos'
//        );
//
//        $params = array(
//            'access_token' => $this->getToken(),
//            'seccion' => 'procesados'
//        );
//
//        return $this->getData($url, $params);
//    }
    /**
     * Get shipment information of a specific shipment
     * @param $shipmentId int
     * @return array
     */
    public function getShipmentInformation($shipmentId)
    {
        //Example: https://api.enviopack.com/envios/17151?access_token=[TU_ACCESS_TOKEN]
        $url = sprintf(
            "%senvios/%s",
            $this->_apiUrl,
            $shipmentId
        );

        $params = array(
            'access_token' => $this->getToken()
        );

        return $this->getData($url, $params);
    }

    /**
     * Get branches by shipping option and locality
     * @param $shippingMethodId
     * @param null $localityId
     * @return array
     */
    public function getBranches($shippingMethodId, $localityId = null)
    {
        /** @var DrubuNet_EnvioPack_Helper_Data $helper */
        $helper = Mage::helper('drubunet_enviopack');

        $region = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getRegion();
        $region = $helper->mapProvince($region);

//        $url = sprintf(
//            "%s%s",
//            $this->_apiUrl,
//            'sucursales'
//        );
//
//        $params = array(
//            'access_token' => $this->getToken(),
//            'id_correo' => $shippingMethodId,
//            'id_localidad' => $localityId
//        );
//
//        return $this->getData($url, $params);

        $url         = $this->_apiUrl . 'localidades';
        $accessToken = $this->getToken();
        $content     = array(
            'access_token' => $accessToken,
            'id_correo'    => $shippingMethodId,
            'id_provincia' => $region,
        );
        $localidades = $this->getData($url, $content);

        $result = array();
        foreach ($localidades as $localidad) {
            $url = $this->_apiUrl . 'sucursales';

            $content = array(
                'access_token' => $accessToken,
                'id_correo' => $shippingMethodId,
                'id_localidad' => $localidad['id']
            );

            $sucursales = $this->getData($url, $content);

            foreach ($sucursales as $sucursal) {
                $result[] = array(
                    'id' => $sucursal['id'],
                    'nombre' => sprintf(
                        "%s | %s (%s %s)",
                        $localidad['nombre'],
                        $sucursal['nombre'],
                        $sucursal['calle'],
                        $sucursal['numero']
                    ) ,
                );
            }
        }

        return $result;
    }

    /**
     * Get tracking information of a specific shipment
     * @param $shipmentId int
     * @return array
     */
    public function getTrackingInformation($shipmentId)
    {
        //Example: https://api.enviopack.com/envios/191/tracking?access_token=[TU_ACCESS_TOKEN]
        $url = sprintf(
            "%senvios/%s/tracking",
            $this->_apiUrl,
            $shipmentId
        );

        $params = array(
            'access_token' => $this->getToken()
        );

        return $this->getData($url, $params);
    }

    /**
     * Get shipping label for a specific shipment
     * @param $shipmentIds string
     * @param $formatLabel string
     * @return array
     */
    public function getShippingLabel($shipmentIds, $formatLabel = 'pdf')
    {
        //Example: 'https://api.enviopack.com/envios/320/etiqueta?access_token=[TU_ACCESS_TOKEN]&formato=pdf'
        $url = sprintf(
            "%senvios/etiquetas",
            $this->_apiUrl
        );

        $result = '';
        try {
            $result = $this->getData(
                $url,
                array(
                    'access_token' => $this->getToken(),
                    'ids' => $shipmentIds
                ),
                false
            );
        } catch (Exception $e) {
            Mage::log($e->getMessage(),null, 'enviopack.log');
        }

        return $result;
    }

    /**
     * Prepare data required to create EnvioPack Shipment
     * @param $order Mage_Sales_Model_Order
     * @param $envioPackId int
     * @return array
     */
    private function prepareData($order, $envioPackId)
    {
        /** @var DrubuNet_EnvioPack_Helper_Data $helper */
        $helper = Mage::helper('drubunet_enviopack');

        /** @var DrubuNet_EnvioPack_Helper_Measurement $measurement */
        $measurement = Mage::helper('drubunet_enviopack/measurement');

        $shippingMethod = $order->getShippingMethod();

        $shippingInfo = explode('_', $shippingMethod);

        $shippingAddress = $order->getShippingAddress();

        $addressFromId = $helper->getAddressFromId();

        $shipmentType = strtoupper($shippingInfo[self::SHIPPING_INFO_TYPE]);

        $shippingName = ($shippingInfo[self::SHIPPING_INFO_SHIPPING_NAME] == 'custom' ? '' : $shippingInfo[self::SHIPPING_INFO_SHIPPING_NAME]);

        $serviceType = strtoupper($shippingInfo[self::SHIPPING_INFO_SERVICE]);

        $quoteBy = $helper->getConfig('quote_by');
        if($quoteBy == DrubuNet_EnvioPack_Model_Quote::DIMENSIONS) {
            $collection = $order->getAllItems();
            $sizes = $measurement->getSizes($collection);
            $alto = $sizes['height'];
            $ancho = $sizes['width'];
            $largo = $sizes['length'];
            $peso = $sizes['weight'];
        } else {
            $packageId = $helper->getPackageId();
            $weightUnit = $helper->getWeightUnit();
            /** @var DrubuNet_EnvioPack_Model_Packages $api */
            $api = Mage::getModel('drubunet_enviopack/packages');
            $packageInfo = $api->getPackages($packageId);
            $alto = $packageInfo['alto'];
            $ancho = $packageInfo['ancho'];
            $largo = $packageInfo['largo'];
            //$peso = $packageInfo['peso'];
            if ($weightUnit == $measurement::ENVIOPACK_GR_CODE) {
                //$peso = $measurement->getValueWeightConversion($weightUnit);
                $peso = $measurement->getValueWeightConversion($order->getWeight());
            } else {
                $peso = $order->getWeight();
            }
        }

        //$confirmedShipment = (bool)$helper->getIsConfirmedShipment();

        $content = array(
            'pedido' => $envioPackId,
            'direccion_envio' => $addressFromId,
            'destinatario' => $shippingAddress->getName(),
            'observaciones' => '',
            'modalidad' => $shipmentType,
            'servicio' => $serviceType,
            //'correo' => $shippingName,
            'confirmado' => true,//$confirmedShipment,
            'paquetes' => array(
                array('alto' => $alto, 'ancho' => $ancho, 'largo' => $largo, 'peso' => $peso)
            ),

        );

        if ($shippingName) {
            $content['correo'] = $shippingName;
        }

        if ($shipmentType == self::ENVIO_DOMICILIO) {
            //Si es a domicilio y generacion automatica confirmado = false
            if (!$shippingName) {
                $content['confirmado'] = false;
            }

            $address = array(
                'calle'         => $shippingAddress->getStreet1(),
                'numero'        => $shippingAddress->getStreet2(),
                'piso'          => $shippingAddress->getStreet3(),
                'depto'         => $shippingAddress->getStreet4(),
                'codigo_postal' => $shippingAddress->getPostcode(),
                'provincia'     => $helper->mapProvince($shippingAddress->getRegion()),
                'localidad'     => $shippingAddress->getCity()
            );

            return array_merge($content, $address);
        } else {
            $content['sucursal'] = $order->getEnviopackBranch();
            return $content;
        }
    }
}
