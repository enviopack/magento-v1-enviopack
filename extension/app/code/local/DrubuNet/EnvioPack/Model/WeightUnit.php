<?php
/**
 * Created by PhpStorm.
 * User: Pablo Garcia
 * Date: 24/08/17
 * Time: 21:14
 */
class DrubuNet_EnvioPack_Model_WeightUnit extends DrubuNet_EnvioPack_Model_Core
{
    /**
     *
     * @return array
     */
    public function toOptionArray()
    {
        /** @var DrubuNet_EnvioPack_Helper_Measurement $measurement */
        $measurement = Mage::helper('drubunet_enviopack/measurement');

        return array(
            array('value'=> $measurement::ENVIOPACK_GR_CODE, 'label'=> $measurement::ENVIOPACK_GR_LABEL),
            array('value'=> $measurement::ENVIOPACK_KG_CODE, 'label'=> $measurement::ENVIOPACK_KG_LABEL)
        );
    }
}