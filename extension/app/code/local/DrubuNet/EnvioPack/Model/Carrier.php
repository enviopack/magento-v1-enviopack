<?php
/**
 * Modified by Atom.
 * User: Santiago de la Fuente
 * Company: Enviopack
 * Date: 07/09/2020
 */
class DrubuNet_EnvioPack_Model_Carrier extends Mage_Shipping_Model_Carrier_Abstract implements Mage_Shipping_Model_Carrier_Interface {

    protected $_code = 'drubunet_enviopack';

    /**
     * Collect shipping rates to show in checkout
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return bool|Mage_Shipping_Model_Rate_Result
     */
    public function collectRates(
        Mage_Shipping_Model_Rate_Request $request
    )
    {
        if (!$this->getConfigFlag('active')) return false;

        return $this->_getRates($request);

    }

    /**
     * Get rates from envioPack api
     * @param Mage_Shipping_Model_Rate_Request $request
     * @return Mage_Shipping_Model_Rate_Result
     */
    protected function _getRates($request)
    {
        /* @var $result Mage_Shipping_Model_Rate_Result */
        $result = Mage::getModel('shipping/rate_result');

        /** @var $api DrubuNet_EnvioPack_Model_Rates */
        $api = Mage::getModel('drubunet_enviopack/rates');

        /** @var DrubuNet_EnvioPack_Helper_Data $helper */
        $helper = Mage::helper('drubunet_enviopack');

        /** @var DrubuNet_EnvioPack_Helper_Measurement $measurement */
        $measurement = Mage::helper('drubunet_enviopack/measurement');
        // $packageHeight = $request->getPackageHeight();
        // $packageWeight = $request->getPackageWeight();
        // $packageDepth = $request->getPackageDepth();
        //
        // $volumen = $packageHeight * $packageWeight * $packageDepth;
        $region  = Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getRegion();
        $content = array(
            'codigoPostal'  => $request->getDestPostcode(),
            'peso'          => $request->getPackageWeight(),
            'provincia'     => $helper->mapProvince($region)
        );

        $quoteBy = $helper->getConfig('quote_by');
        if($quoteBy == DrubuNet_EnvioPack_Model_Quote::DIMENSIONS) {
            $collection         = $request->getAllItems();
            $sizes              = $measurement->getSizes($collection);
            $content['volumen'] = $sizes['height'] * $sizes['width'] * $sizes['length'];
        }

        $rates = $api->getRatesByProvince($content);

        foreach ($rates as $rate) {
            $result->append($this->_getRate($rate));
        }

        return $result;
    }

    /**
     * Format rate to be added to result rates
     * @param $data array
     * @return false|Mage_Core_Model_Abstract|Mage_Shipping_Model_Rate_Result_Method
     */
    protected function _getRate($data) {
        /* @var $rate Mage_Shipping_Model_Rate_Result_Method */
        $rate = Mage::getModel('shipping/rate_result_method');

        /** @var DrubuNet_EnvioPack_Helper_Data $helper */
        $helper = Mage::helper('drubunet_enviopack');

        $rate->setCarrier($this->_code);
        /**
         * getConfigData(config_key) returns the configuration value for the
         * carriers/[carrier_code]/[config_key]
         */
        $rate->setCarrierTitle($this->getConfigData('title'));

        $mode = $helper->mapMode($data['modalidad']);
        $service = $helper->mapService($data['servicio']);
        $deliveryTime = $data['horas_entrega'];

        if(isset($data['correo'])) {
            $carrierName = $data['correo']['nombre'];
            $carrierId = $data['correo']['id'];

            $title = sprintf("%s - %s - %s (%s hs.)", $carrierName, $mode, $service, $deliveryTime);
            $method = sprintf(
                "%s_%s_%s_%s",
                strtolower($data['modalidad']),
                strtolower($data['servicio']),
                $carrierId,
                ((int)$data['valor'])
            );
        } else {
            $carrierId = 'custom';

            $title = sprintf("%s - %s (%s hs.)", $mode, $service, $deliveryTime);
            $method = sprintf(
                "%s_%s_%s",
                strtolower($data['modalidad']),
                strtolower($data['servicio']),
                $carrierId
            );
        }



        $rate->setMethod($method);
        $rate->setMethodTitle($title);

        $rate->setPrice($data['valor']);
        $rate->setCost($data['valor']);

        return $rate;
    }

    public function getAllowedMethods() {
        return array(
            'standard' => 'Standard',
            'express' => 'Express',
            'free_shipping' => 'Free Shipping',
        );
    }
}
