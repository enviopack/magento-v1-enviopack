<?php
/**
 * Created by PhpStorm.
 * User: Pablo Garcia
 * Date: 16/08/17
 * Time: 21:14
 */

class DrubuNet_EnvioPack_Model_Quote extends DrubuNet_EnvioPack_Model_Core
{
    const PACKAGE = 1;
    const DIMENSIONS = 2;
    /**
     *
     * @return array
     */
    public function toOptionArray()
    {
        $result = array(
            array(
                'value'=> self::PACKAGE,
                'label'=> 'Paquete'
            ),
            array(
                'value'=> self::DIMENSIONS,
                'label'=> 'Dimensiones de Productos'
            )
        );

        return $result;
    }
}