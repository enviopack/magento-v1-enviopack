<?php
class DrubuNet_EnvioPack_CheckoutController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $localityId = $this->getRequest()->getParam('locality_id', false);
        $service = $this->getRequest()->getParam('service', false);
        //$branchCollection = array();
        $options = array();

        if ($localityId && $service) {
            /** @var DrubuNet_EnvioPack_Model_Shipment $api */
            $api = Mage::getModel('drubunet_enviopack/shipment');

            $branchCollection = $api->getBranches($service, $localityId);

            foreach ($branchCollection as $branch) {
                $options[] = array(
                    'key' =>  $branch['id'],
                    'value' =>  $branch['nombre'].' - ('.$branch['calle'] . ' '. $branch['numero'].')'
                );
            }

            if (count($options)) {
                $ok = true;
            } else {
                $ok = false;

            }

        } else {
            $ok = false;
        }

        $result = array (
            'ok' => $ok,
            'data' => $options
        );

        echo json_encode($result);
    }
}