<?php
class DrubuNet_EnvioPack_Block_Adminhtml_Orders_Renderer_Shipment extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    public function render(Varien_Object $row) {

        $shippingMethod = $row->getShippingMethod();
        $shippingInfo = explode('_', $shippingMethod);
        $shippingName = $shippingInfo[DrubuNet_EnvioPack_Model_Core::SHIPPING_INFO_SHIPPING_NAME];
        if(!$row->getEnviopackShipmentId() && $shippingName == 'custom') {
            /** @var $api DrubuNet_EnvioPack_Model_Rates */
            $api = Mage::getModel('drubunet_enviopack/rates');
            $orderId = $row->getId();

            /** @var Mage_Sales_Model_Order $row */
            //$order = Mage::getModel('sales/order')->load($orderId);
            $shippingAddress = $row->getShippingAddress();

            $orderBy = 'valor';
            if (Mage::registry('order_by')) {
                $orderBy = Mage::registry('order_by');
            }

            $data = array(
                'postal_code' => $shippingAddress->getPostcode(),
                'weight' => $row->getWeight(),
                'mode' => 'D',
                'order_by' => $orderBy,
                'sort' => 'asc'
            );

            $rates = $api->getCostRates($data);

            $html = '<select id="rate_'.$orderId.'" name="rate_'.$orderId.'">';
            foreach ($rates as $rate) {
                $html.= $this->renderOption($rate);
            }
            $html.='</select>';

        } else {
            $html = 'N/A';
        }

        return $html;
    }

    private function renderOption($rate)
    {
        /** @var DrubuNet_EnvioPack_Helper_Data $helper */
        $helper = Mage::helper('drubunet_enviopack');

        //$value = $rate['correo']['id'];

        $value = sprintf(
            "%s_%s_%s",
            strtolower($rate['modalidad']),
            strtolower($rate['servicio']),
            $rate['correo']['id']
        );

        $service = $helper->mapService($rate['servicio']);
        $deliveryTime = $rate['horas_entrega'];
        $price = $rate['valor'];
        $carrierName = $rate['correo']['nombre'];

        $label = sprintf(
            "$%s | %s - %s hs. - %s",
            $price,
            $service,
            $deliveryTime,
            $carrierName
        );

        $option = sprintf(
            "<option value=\"%s\">%s</option>",
            $value,
            $label
        );

        return $option;

    }
}