<?php
class DrubuNet_EnvioPack_Block_Adminhtml_Orders_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();

        // Set some defaults for our grid
        $this->setDefaultSort('entity_id');
        $this->setId('drubunet_enviopack_orders_grid');
        $this->setDefaultDir('desc');
        $this->setSaveParametersInSession(true);
    }

    protected function _getCollectionClass()
    {
        // This is the model we are using for the grid
        return 'sales/order';
    }

    protected function _prepareCollection()
    {
        // Get and set our collection for the grid
        $collection = Mage::getModel('sales/order')->getCollection();

        $collection->addAttributeToFilter(
            'shipping_method',
            array(
                'like' => 'drubunet_enviopack_%'
            )
        );

        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('envio_pack_entity_id');
        $this->getMassactionBlock()->setFormFieldName('entity_id');

        $this->getMassactionBlock()->addItem(
            'print_labels',
            array(
                'label' => $this->__('Imprimir Etiquetas'),
                'url' => $this->getUrl('*/*/massPrint', array('' => '')),
                'confirm' => Mage::helper('tax')->__('Imprimir Etiquetas?')
            )
        );

        $this->getMassactionBlock()->addItem(
            'generate_shipment',
            array(
                'label' => $this->__('Generar Envios'),
                'url' => $this->getUrl('*/*/massShipment', array('' => '')),
                'confirm' => Mage::helper('tax')->__('Seguro que desea procesar los seleccionados?')
            )
        );

        return $this;
    }

    protected function _prepareColumns()
    {
        // Add the columns that should appear in the grid
        $this->addColumn('entity_id',
            array(
                'header'=> $this->__('Orden ID'),
                'align' =>'right',
                'width' => '50px',
                'index' => 'entity_id'
            )
        );

        $this->addColumn('status', array(
            'header' => Mage::helper('sales')->__('Status'),
            'index' => 'status',
            'type'  => 'options',
            'width' => '70px',
            'options' => Mage::getSingleton('sales/order_config')->getStatuses(),
        ));

        $this->addColumn('enviopack_shipment_id',
            array(
                'header'=> $this->__('#Envio'),
                'index' => 'enviopack_shipment_id',
            )
        );
        $this->addColumn('increment_id',
            array(
                'header'=> $this->__('Order #'),
                'index' => 'increment_id',
            )
        );

        $this->addColumn('customer_firstname',
            array(
                'header'=> $this->__('Nombre'),
                'index' => 'customer_firstname',
            )
        );

        $this->addColumn('customer_lastname',
            array(
                'header'=> $this->__('Apellido'),
                'index' => 'customer_lastname',
            )
        );

        $this->addColumn('shipping_description',
            array(
                'header'=> $this->__('Shipping Description'),
                'index' => 'shipping_description',
            )
        );

//        $this->addColumn('enviopack_branch',
//            array(
//                'header'=> $this->__('Sucursal'),
//                'index' => 'enviopack_branch',
//            )
//        );
        $this->addColumn('enviopack_modalidad',
            array(
                'header'=> $this->__('Modalidad'),
                'renderer'=> 'DrubuNet_EnvioPack_Block_Adminhtml_Orders_Renderer_Mode'
            )
        );

        $this->addColumn('print_label',
            array(
                'header'=> $this->__('Imprimir Etiqueta'),
                'renderer'=> 'DrubuNet_EnvioPack_Block_Adminhtml_Orders_Renderer_Label'
            )
        );

        $this->addColumn('tracking_information',
            array(
                'header'=> $this->__('Tracking Information'),
                'renderer'=> 'DrubuNet_EnvioPack_Block_Adminhtml_Orders_Renderer_Tracking'
            )
        );

        $this->addColumn('action_shipment',
            array(
                'header'=> $this->__('Create Shipment'),
                'renderer'=> 'DrubuNet_EnvioPack_Block_Adminhtml_Orders_Renderer_Shipment'
            )
        );

        $this->addColumn('processed',
            array(
                'header'=> $this->__('Procesado'),
                'renderer'=> 'DrubuNet_EnvioPack_Block_Adminhtml_Orders_Renderer_Processed'
            )
        );


        return parent::_prepareColumns();
    }

    public function getRowUrl($row)
    {

    }
}