<?php

class DrubuNet_EnvioPack_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Get Extension's config
     * @param $key
     * @return mixed
     */
    public function getConfig($key)
    {
        return Mage::getStoreConfig('carriers/drubunet_enviopack/' . $key);
    }

    /**
     * @return int
     */
    public function getWeightUnit()
    {
        return $this->getConfig('weight_unit');
    }
    /**
     * Get Address From Id
     * @return int
     */
    public function getAddressFromId()
    {
        return $this->getConfig('address_from_id');
    }

    /**
     * Get Package Id
     * @return int
     */
    public function getPackageId()
    {
        return $this->getConfig('package_id');
    }

    /**
     * Get Is Confirmed Shipment
     * @return boolean
     */
    public function getIsConfirmedShipment()
    {
        return $this->getConfig('confirmed_shipment');
    }

    public function getBranchesData()
    {
        $result = Mage::registry('branches');
        return $result;
    }

    /**
     * Map mode from code to string
     * @param $mode string
     * @return string
     */
    public function mapMode($mode)
    {
        /*
         * Los valores posibles son:
            - D: para envíos a domicilio
            - S: para envíos a sucursal
         */
        $result = array(
            'D' => 'Envio a Domicilio',
            'S' => 'Envio a Sucursal'

        );
        return $result[$mode];
    }

    /**
     * Map service from code to string
     * @param $service string
     * @return mixed string
     */
    public function mapService($service)
    {
        /*
         * Los valores posibles son:
            - N: para el servicio estándar
            - P: para el servicio prioritario
            - X: para el servicio express
            - R: para el servicio de devoluciones
            - C: para el servicio de cambios
        */
        $result = array(
            'N' => 'Estandar',
            'P' => 'Prioritario',
            'X' => 'Express',
            'R' => 'Devoluciones',
            'C' => 'Cambios'
        );
        return $result[$service];
    }

    /**
     * Create Magento Shipment
     * @param Mage_Sales_Model_Order $order
     */
    public function createMagentoShipment($order)
    {
        if (!count($order->getShipmentsCollection())) {
            try {
                $itemQty =  $order->getItemsCollection()->count();
                $shipment = Mage::getModel('sales/service_order', $order)->prepareShipment($itemQty);
                $shipment = new Mage_Sales_Model_Order_Shipment_Api();
                $shipmentId = $shipment->create( $order->getIncrementId(), array(), '');

            }catch (Exception $e) {
                Mage::log('Ha ocurrido un error al tratar de imponer la orden n° '.$order->getIncrementId().' error: '.$e->getMessage(),null, 'envioPack.log');
            }
        }
    }

    /**
     * Map Magento Region Code to ISO_3166-2:AR without AR- prefix.
     * @param $regionCode
     * @return string
     */
    private function mapProvince($regionSelected)
    {
        $provinceList = [
            'Salta'                           =>'A',
            'Buenos Aires'                    =>'B',
            'Ciudad Autónoma de Buenos Aires' =>'C',
            'San Luis'                        =>'D',
            'Entre Ríos'                      =>'E',
            'La Rioja'                        =>'F',
            'Santiago del Estero'             =>'G',
            'Chaco'                           =>'H',
            'San Juan'                        =>'J',
            'Catamarca'                       =>'K',
            'La Pampa'                        =>'L',
            'Mendoza'                         =>'M',
            'Misiones'                        =>'N',
            'Formosa'                         =>'P',
            'Neuquén'                         =>'Q',
            'Río Negro'                       =>'R',
            'Santa Fe'                        =>'S',
            'Tucumán'                         =>'T',
            'Chubut'                          =>'U',
            'Tierra del Fuego'                =>'V',
            'Corrientes'                      =>'W',
            'Córdoba'                         =>'X',
            'Jujuy'                           =>'Y',
            'Santa Cruz'                      =>'Z',
        ];

       return (isset($provinceList[$regionSelected])) ? $provinceList[$regionSelected] : 'C';
    }
}
