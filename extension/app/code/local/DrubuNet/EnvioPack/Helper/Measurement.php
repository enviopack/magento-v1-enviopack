<?php
/**
 * Created by PhpStorm.
 * User: Pablo Garcia
 * Email: pgarcia@drubu.net
 * Date: 17/08/17
 * Time: 20:17
 */

class DrubuNet_EnvioPack_Helper_Measurement extends Mage_Core_Helper_Abstract
{
    const ENVIOPACK_GR_CODE = 1;
    const ENVIOPACK_KG_CODE = 2;
    const ENVIOPACK_GR_LABEL = 'Gramos';
    const ENVIOPACK_KG_LABEL = 'Kilogramos';

    const ENVIOPACK_LENGTH_UNIT = 'cm';
    const ENVIOPACK_WEIGHT_UNIT = 'kg';

    const ENVIOPACK_LENGTH_NAME = 'length';
    const ENVIOPACK_HEIGHT_NAME = 'height';
    const ENVIOPACK_WIDTH_NAME = 'width';
    const ENVIOPACK_WEIGHT_NAME = 'weight';

    /** @var DrubuNet_EnvioPack_Helper_Data $_helper  */
    protected $_helper;
    /** @var  array $_mapping */
    protected $_mapping;

    public function __construct()
    {
        $this->_helper = Mage::helper('drubunet_enviopack');
    }

    /**
     * @param array $collection
     * @return array
     */
    public function getSizes($collection)
    {
        //$return = array();
        $totalLength = 0;
        $totalHeight = 0;
        $totalWidth = 0;
        $totalWeight = 0;

        /** @var Mage_Sales_Model_Quote_Item|Mage_Sales_Model_Order_Item $product */
        foreach ($collection as $product) {
            /** @var Mage_Catalog_Model_Product $_product */
            $_product = Mage::getModel('catalog/product')->load($product->getProductId());
            $productLength = $this->getMeasurementValue($_product, self::ENVIOPACK_LENGTH_NAME);
            $productHeight = $this->getMeasurementValue($_product, self::ENVIOPACK_HEIGHT_NAME);
            $productWidth  = $this->getMeasurementValue($_product, self::ENVIOPACK_WIDTH_NAME);

            if ( $productLength >  $totalLength ) {
                $totalLength  = $productLength;
            }

            if ( $productHeight >  $totalHeight ) {
                $totalHeight  = $productHeight;
            }

            if ( $productWidth >  $totalWidth ) {
                $totalWidth = $productWidth;
            }

            $totalWeight += $this->getMeasurementValue($_product, self::ENVIOPACK_WEIGHT_NAME);
        }

        $return = array(
            "height"  => ceil($totalHeight),
            "width" => ceil($totalWidth),
            "length" => ceil($totalLength),// * count($collection),
            //"weight"  => $totalWeight
        );

        $mins = array_keys($return, min($return));

        if(count($mins) == 1) {
            $return[$mins[0]] = $return[$mins[0]] * count($collection);
        } else if(count($mins) > 1) {
            $return["length"] = ceil($totalLength) * count($collection);
        }

        $return["weight"] = $totalWeight;

        return $return;
    }

    /**
     * @param Mage_Catalog_Model_Product $product
     * @param string $unit
     * @return float
     */
    protected function getMeasurementValue($product, $unit)
    {
        $attribute = $this->getAttributeByUnit($unit);
        $attributeName = $attribute['code'];
        $attributeValue = $product->getData($attributeName);
        if (!$attributeValue) {
            $value = 0;
        } else {
            $value = $this->getValueUnitConversion($attribute['code'], $attributeValue);
        }

        return $value;
    }

    protected function getAttributeByUnit($unit)
    {
        $attributeMapping = $this->getAttributeMapping();
        return $attributeMapping[$unit];
    }

    protected function getAttributeMapping()
    {
        if (!$this->_mapping) {
            $mapping = $this->_helper->getConfig('attributesmapping');
            $mapping = unserialize($mapping);
            $mappingResult = [];
            foreach ($mapping as $key => $map) {
                $mappingResult[$key] = ['code' => $map['attribute_code'], 'unit' => $map['unit']];
            }
            $this->_mapping = $mappingResult;
        }

        return $this->_mapping;
    }

    protected function getValueUnitConversion($attributeType, $value)
    {
        if ($attributeType == self::ENVIOPACK_WEIGHT_NAME) {
            //check if needs conversion
            if ($this->_mapping[$attributeType]['unit'] != self::ENVIOPACK_WEIGHT_UNIT) {

                return $this->getValueWeightConversion($value);
            }

        } elseif ($this->_mapping[$attributeType]['unit'] != self::ENVIOPACK_LENGTH_UNIT) {
            $unit = new Zend_Measure_Length((float)$value);
            $unit->convertTo(Zend_Measure_Length::CENTIMETER);

            return $unit->getValue();
        }

        return $value;
    }
    public function getValueWeightConversion($value)
    {
        $unit = new Zend_Measure_Weight((float)$value, Zend_Measure_Weight::GRAM);
        $unit->convertTo(Zend_Measure_Weight::KILOGRAM);

        return $unit->getValue();
    }
    public function getUnitWeight()
    {
        $attribute = $this->getAttributeByUnit('weight');
        return $attribute;
    }
}